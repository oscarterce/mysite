# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class categorias(models.Model):
    nombre = models.CharField(max_length=140, verbose_name = "nombre", )
    estado = models.BooleanField(default=True,verbose_name = 'estado')
    def __unicode__(self):
        return "%s" % (self.nombre)
class publicaciones(models.Model):
    """CATEGORIA = (
        (1,'politica'),
        (2,'futbol'),
    )"""
    categoria = models.ForeignKey('blog.categorias',verbose_name = 'categoria', default=True,blank=True,)
    titulo = models.CharField(max_length=140, verbose_name = "titulo", )
    contenido = models.TextField(verbose_name = 'contenido',)
    estado = models.BooleanField(default=True,verbose_name = 'estado')
    fecha = models.DateTimeField(auto_now_add=True,blank=True, null=True,verbose_name = 'fecha de registro',)

class comentarios(models.Model):
    publicacion = models.ForeignKey('blog.publicaciones',verbose_name = 'publicacion', )
    contenido = models.TextField(verbose_name = 'comentario',)
    estado = models.BooleanField(default=True,verbose_name = 'estado')
    fecha = models.DateTimeField(auto_now_add=True,blank=True, null=True,verbose_name = 'fecha de registro',)