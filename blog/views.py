# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import TemplateView,View
from blog.models import publicaciones as mpublicaciones
from blog.models import comentarios
from blog.forms import comentariosForm
# Create your views here.
class inicio(TemplateView):
	template_name='inicio.html'
	def get_context_data(self, **kwargs):
		context = super(inicio, self).get_context_data(**kwargs)
		# Tupla , List, Dictionary
		xtupla=('oscar','daniel','luis')
		xlist=[1,2,3]
		xdictionary=[
			{'nombre':'Juan','edad':'32'},
			{'nombre':'Marco','edad':'32'},
		]
		xposts = [
			{'id':1,'titulo':'Juan','contenido':'cualquier cosa...'},
			{'id':2,'titulo':'Giovanna','contenido':'cualquier cosa...'},
			{'id':3,'titulo':'Milán','contenido':'cualquier cosa...'},
			{'id':4,'titulo':'Renato','contenido':'cualquier cosa...'},
		]
		publicaciones = mpublicaciones.objects.filter(estado=True)
		context['publicaciones']=publicaciones
		context['comentariosForm']=comentariosForm
		return context
class newcomentario(View):
	def post(self, request, *args, **kwargs):
		comentario=comentarios.objects.create(
			publicacion_id=request.POST['publicacionId'],
			contenido=request.POST['contenido'],
		)
		return HttpResponseRedirect('/')