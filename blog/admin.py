# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from blog.models import publicaciones,categorias,comentarios

# Register your models here.

class categoriasAdmin(admin.ModelAdmin):
	list_display = ('nombre','estado',)
class comentariosInline(admin.TabularInline):
	model = comentarios
	#fields = ('contenido','estado','fecha',)
	readonly_fields = ['contenido',]
	extra = 0
		
class publicacionesAdmin(admin.ModelAdmin):
	list_display = ('titulo','estado','fecha',)
	inlines = [comentariosInline,]


admin.site.register(publicaciones, publicacionesAdmin)
admin.site.register(categorias, categoriasAdmin)